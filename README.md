**To run:**

Navigate to the project directory and type

`docker-compose up`

**To view the app:**

Open a web-browser and navigate to

http://localhost:5000

Everytime you refresh the site, the counter will update. In addition, you will be referred to by the username on your windows machine.
